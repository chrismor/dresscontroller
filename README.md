# VU Meter Dress #

This is the source for my "VU Meter" dress.  OK, not the dress per-se, but the wearable electronics involved in making it.

## Shopping List ##

* WS281x LED Modules. (Qty 20) Mine are generic items from tronixlabs.com.au
* Arduino Pro Micro.  (Qty 1) This is the smallest device I could find; and I already had them.
* Microphone board with analogue output. (Qty 1)
* Thin flexible hookup wire.  3 colours. (Qty 2m/6'6" each)
* 1000mAh USB Battery bank.  (Qty 1)  Use a small, lightweight device.

## Setting up ##

By default, there are two strings of WS821x LED Modules, 10 LEDS per string.  These string are connected to Pin 3 and pin 4.  Power is derived from the onboard regulator on the Arduino, as is the Ground.

Solder the LEDs together so that Dout on one LED connects to Din on the next,  +5 to +5 and Gnd to Gnd.  You can use the "cycle()" routine in the code as a lamp test function.  Be warned that the connectors on the LED modules are **very small**, and with my old eyes, it's easy to short an output to ground or power.

Connect the microphone's Aout to A0 on the Arduino.  The microphone's power comes from the Arduino, just like the LED strings.

Also for neatness, the Dout -> Din wire should be about 1mm shorter than the power wires because of the circular shape of the modules.

For reliability, I soldered the wires from the LED strings directly to the pads on the Arduino Pro Micro (It has neither pins nor sockets installed), then put the whole assembly inside a large piece of heatshrink.  If you're powering the board via the USB connector as I did, put the cable in place before you warm up the heatshrink (*like I didn't*), because it's tough to connect it later.

The strings of LED modules were fixed to a length of black felt using hot-melt glue on the connecting wires.  By overlapping two pieces of felt I was able to make a pocket on the back to hold the Arduino and battery pack.

With everything assembled, adjust the gain on the microphone board to get a visually pleasing display.  This is not a calibrated instrument, so it's more important to get a visually pleasing rather than technically accurate result.

Finally, it looks better if you're dancing, but be warned it will attract lots of attention from the other kids.