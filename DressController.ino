/*
 * Copyright (C) 2017, Christopher F. Moran
 * 
 * Simple Wearable LED Display.  Emulates a VU Meter
 */

#include <bitswap.h>
#include <chipsets.h>
#include <color.h>
#include <colorpalettes.h>
#include <colorutils.h>
#include <controller.h>
#include <cpp_compat.h>
#include <dmx.h>
#include <FastLED.h>
#include <fastled_config.h>
#include <fastled_delay.h>
#include <fastled_progmem.h>
#include <fastpin.h>
#include <fastspi.h>
#include <fastspi_bitbang.h>
#include <fastspi_dma.h>
#include <fastspi_nop.h>
#include <fastspi_ref.h>
#include <fastspi_types.h>
#include <hsv2rgb.h>
#include <led_sysdefs.h>
#include <lib8tion.h>
#include <noise.h>
#include <pixelset.h>
#include <pixeltypes.h>
#include <platforms.h>
#include <power_mgt.h>

#define NUM_LEDS    10
#define DATA_PIN_0  3
#define DATA_PIN_1  4

CRGB leds0[NUM_LEDS];
CRGB leds1[NUM_LEDS];

void setup() {
  Serial.begin(38400);
  FastLED.addLeds<NEOPIXEL, DATA_PIN_0>(leds0, NUM_LEDS);
  FastLED.addLeds<NEOPIXEL, DATA_PIN_1>(leds1, NUM_LEDS);

  cycle();
}

void cycle() {
  for(int i = 0; i < NUM_LEDS; i++) {
    leds0[i] = CRGB::Red;
    leds1[i] = CRGB::Red;
    FastLED.show();
    delay(50);
    leds0[i] = CRGB::Green;
    leds1[i] = CRGB::Blue;
    FastLED.show();
    delay(50);
    leds0[i] = CRGB::Blue;
    leds1[i] = CRGB::Green;
    FastLED.show();
    delay(50);
    leds0[i] = CRGB::Black;
    leds1[i] = CRGB::Black;
    FastLED.show();
    delay(50);
  }
}

void loop() {
//  int signal = map(analogRead(A0), 0, 10, 0, 20);
  int signal = analogRead(A0) / 25;

  if(signal >= NUM_LEDS) {
    signal = NUM_LEDS;
  }
  
  for(int i = 0; i < NUM_LEDS; i++) {
    if(i <= signal) {
      switch(i) {
        case NUM_LEDS - 2:
        case NUM_LEDS - 1:
          leds0[i] = CRGB::Red;
          leds1[i] = CRGB::Red;
          break;
        case NUM_LEDS - 4:
        case NUM_LEDS - 3:
          leds0[i] = CRGB::Orange;
          leds1[i] = CRGB::Orange;
          break;
        case NUM_LEDS - 6:
        case NUM_LEDS - 5:
          leds0[i] = CRGB::Yellow;
          leds1[i] = CRGB::Yellow;
          break;
        case NUM_LEDS - 8:
        case NUM_LEDS - 7:
          leds0[i] = CRGB::Blue;
          leds1[i] = CRGB::Blue;
          break;
        default:
          leds0[i] = CRGB::Green;
          leds1[i] = CRGB::Green;
          break;
      }
    }
    else {
      leds0[i] = CRGB::Black;
      leds1[i] = CRGB::Black;
    }
    FastLED.show();
    delay(1);
  }
}

